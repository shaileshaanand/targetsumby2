import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.IOException;
public class Main {
    public static void main(String[] args)throws IOException {
        int count=0,sum,number,difference,lastnum;
        ArrayList<Integer> numbers=new ArrayList<>();
        ArrayList<HashSet<Integer>> pairs=new ArrayList<>();
        HashSet<Integer> pair;
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("You can enter e or exit to exit");
        while (true) {
            try {
                System.out.print("Enter a number :");
                number=Integer.parseInt(in.readLine());
                numbers.add(number);
            } catch (Exception e) {
                break;
            }
        }
        System.out.print("Enter the sum to be found out: ");
        sum=Integer.parseInt(in.readLine());
        System.out.println("Size of Array is "+numbers.size());
        Collections.sort(numbers);//Merge Sort
        while (!(numbers.isEmpty() || numbers.size()==1)) {
            number=numbers.get(0);
            numbers.remove(new Integer(number));
            difference=sum-number;
            while (!(numbers.isEmpty() )) {
                lastnum=numbers.get(numbers.size()-1);
                count++;
                if (difference==lastnum) {
                    pair=new HashSet<>();
                    pair.add(number);
                    pair.add(difference);
                    pairs.add(pair);
                    numbers.remove(new Integer(lastnum));
                } else if (difference<lastnum) {
                    numbers.remove(new Integer(lastnum));
                } else {
                    break;
                }
            }
        }
        System.out.println("Sets are:");
        for(HashSet<Integer> pair2:pairs) {
            System.out.println(pair2);
        }
        System.out.println("The inner loop ran "+count+" times");
    }
}

